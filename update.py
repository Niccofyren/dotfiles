#! /usr/bin/env python
import sys, os, shutil, distutils.core

try:
    installPath = sys.argv[1]
except IndexError:
    installPath = os.path.expanduser('~')

installPath = os.path.abspath(installPath)
scriptPath = os.path.dirname(os.path.abspath(__file__))
srcPath = scriptPath + '/src'

print('Warning! Updating will overwrite any changes you have made to this copy of the repo and your local vim-files!')
while True:
    prompt = raw_input('Enter \'yes\' if you are sure you want to continue (or Ctrl+C to abort): ')
    if prompt.lower() == 'yes':
        break

print('Updating repo with latest remote version.')
os.system('cd "{0}" && git remote update && git clean -f -d && git reset --hard HEAD'.format(scriptPath))
os.system('cd "{0}" && git checkout neovim && git pull --force'.format(scriptPath))

print('Repository update complete!')
print('Launching install.py script to update local installation.')
os.system('cd "{0}" && ./install.py "{1}"'.format(scriptPath, installPath))

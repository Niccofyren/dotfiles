#! /usr/bin/env python
from shutil import copy2, copystat, Error, ignore_patterns
import sys, os


#Source: http://stackoverflow.com/questions/38876945/copying-and-merging-directories-excluding-certain-extensions
def copytree_multi(src, dst, symlinks=False, ignore=None):
    names = os.listdir(src)
    if ignore is not None:
        ignored_names = ignore(src, names)
    else:
        ignored_names = set()

    # -------- E D I T --------
    # os.path.isdir(dst)
    if not os.path.isdir(dst):
        os.makedirs(dst)
    # -------- E D I T --------

    errors = []
    for name in names:
        if name in ignored_names:
            continue
        srcname = os.path.join(src, name)
        dstname = os.path.join(dst, name)
        try:
            if symlinks and os.path.islink(srcname):
                linkto = os.readlink(srcname)
                os.symlink(linkto, dstname)
            elif os.path.isdir(srcname):
                copytree_multi(srcname, dstname, symlinks, ignore)
            else:
                copy2(srcname, dstname)
        except (IOError, os.error) as why:
            errors.append((srcname, dstname, str(why)))
        except Error as err:
            errors.extend(err.args[0])
    try:
        copystat(src, dst)
    except WindowsError:
        pass
    except OSError as why:
        errors.extend((src, dst, str(why)))
    if errors:
        raise Error(errors)

try:
    installPath = sys.argv[1]
except IndexError:
    installPath = os.path.expanduser('~')



installPath = os.path.abspath(installPath)
scriptPath = os.path.dirname(os.path.abspath(__file__))
srcPath = scriptPath + '/src'
itemsToCheck = ['.vim', '.vimrc', '_vimrc', '.tmux.conf','.eslintrc','.tmux-osx.conf','.zshrc']
itemsToIgnore = ignore_patterns('plugged','undo','swap','config','known_hosts','id_rsa')

print('Fetching local changes from {0}'.format(installPath))
while True:
    prompt = raw_input('Is this location correct? Enter \'yes\' to continue (or Ctrl+C to abort): ')
    if prompt.lower() == 'yes':
        break

for item in itemsToCheck:
    if os.path.isdir(installPath + '/' + item):
        print 'Copying folder: ' + item
        copytree_multi(installPath + '/' + item, srcPath + '/' + item,  False, itemsToIgnore)
    elif os.path.isfile(installPath + '/' + item):
        print 'Copying file: ' + item
        copy2(installPath + '/' + item, srcPath + '/' + item)
    else:
        print 'File not found, skipping: ' + item

print('Checking Git status')
os.system('cd "{0}" && git fetch && git status'.format(scriptPath))


Niccofyren dotfiles
========================

My private dotfiles-setup, based on my personal workflow and preferences. This
incudes usage of tmux instead of screen, z shell instead of bash, nvim instead
of vim, and mac as primary work platform. When I encounter concrete problems I
will strive to keep this configuration compatible (error message free) for
other preferences and platforms.

Feel free to try it out.

Installation instructions
-------------------------

1. Clone to local directory with "git clone ..."
2. Install with python-script:

	- Option A, install by script:

			./install.py <location of folder containing .vimrc>
			# Location defaults to "~/" which is usually correct

	- Option B, install manually:

		1. Copy contents of "src/" (including hidden files) to your home
		   directory
		2. Start nvim/vim and run the :PlugInstall command

3. Set up neovim symlinks ($XDG_CONFIG_HOME =? ~/.config):

		ln -s ~/.vim $XDG_CONFIG_HOME/nvim
		ln -s ~/.vimrc $XDG_CONFIG_HOME/nvim/init.vim

Push local changes to repository
--------------------------------

1. Run ./fetch-local-changes.py or copy changed files manually to "src/"
2. Git fetch, add, commit and push.

True Color support
------------------

With the change to neovim comes new possibilities in terms of true
color support. This is great, but in certain situations be a bit
tricky to get working properly. So here are some general tips.

### Terminal emulators
Recent versions of Gnome Terminal (based on libvte 0.36 or newer)
has true color support enabled by default. Alternatively Konsole
is an easy alternative with built in support.

### Terminal multiplexers
TMUX has true color support since 2.2. This is propably not the
version installed in your distro. To install it from source try the
following:

Download source from:

	https://launchpad.net/ubuntu/+source/tmux

Build:

	./configure
	make
	sudo make install

Check current version with:

	tmux -V

Once installed, true color still needs to be manually enabled.
Easiest way to do this is by creating (or modifying) your
.tmux.conf file, located in your home directory.

Find your normal terminal identifier:

	echo $TERM

Add the following line to .tmux.conf:

	set -ga terminal-overrides ",<name from $TERM>:Tc"


### External information
- [Colours in terminal - XVilka](https://gist.github.com/XVilka/8346728)
- [Adding 24-bit TrueColor RGB escape sequences to tmux](https://sunaku.github.io/tmux-24bit-color.html)
- [Documentation - Neovim](https://neovim.io/doc/)


Python and plugins
------------------

Some plugins behave differently in neovim with regards to python. If
experiencing any weird issues, you might find some of the following
tips and tricks useful.

Make sure python2 and python3 is installed:

	sudo apt-get install python-dev python-pip python3-dev python3-pip


Make sure you have added the python client for Neovim:

	sudo pip2 install neovim
	sudo pip3 install neovim


Misc
----

Fix problem with unresponsive <esc> in tmux, by adding the following
line to your .tmux.conf file:

	set -g escape-time 10

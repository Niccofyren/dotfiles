#! /usr/bin/env python
import sys, os, shutil, distutils.core

try:
    installPath = sys.argv[1]
except IndexError:
    installPath = os.path.expanduser('~')

installPath = os.path.abspath(installPath)
srcPath = os.path.dirname(os.path.abspath(__file__)) + '/src'

print('Installing to {0}'.format(installPath))
while True:
    prompt = raw_input('Is this correct? Enter \'yes\' to continue (or Ctrl+C to abort): ')
    if prompt.lower() == 'yes':
        break

for item in os.listdir(srcPath):
    if os.path.isdir(srcPath + '/' + item):
        print 'Copying folder: ' + item
        distutils.dir_util.copy_tree(srcPath + '/' + item, installPath + '/' + item)
    elif os.path.isfile(srcPath + '/' + item):
        print 'Copying file: ' + item
        shutil.copy2(srcPath + '/' + item, installPath + '/' + item)
    else:
        print 'File not found, skipping: ' + item

print('Installing plugins.')
while True:
    version = raw_input('Are you using vim or nvim? Enter one to continue: ')
    if version.lower() in ['vim', 'nvim']:
        break

os.system('{0} -c ":PlugInstall" -c ":PlugUpdate" -c ":qa"'.format(version))
print('Installation complete!')

export LANG=en_US.UTF-8

# Store default user for later use
export DEFAULT_USER=$(whoami)
export DEFAULT_HOSTNAME=$(hostname)

# Configure zsh
export HISTCONTROL=erasedups  # Ignore duplicate entries in history
export HISTFILE=~/.histfile
export HISTSIZE=10000         # Increases size of history
export SAVEHIST=10000
export EDITOR='nvim'
export HISTIGNORE="&:ls:ll:la:l.:pwd:exit:clear:clr:[bf]g"
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY

export PATH="$PATH:$HOME/.rvm/bin" # Add RVM to PATH for scripting
export PATH="$PATH:$(yarn global bin)" # Add Yarn/Node to PATH for scripting

# Load custom preferences
[ -f ~/.zsh_variables ] && source ~/.zsh_variables

# Enable autocompletion
autoload -U compinit && compinit
zmodload -i zsh/complist

# Enable vim keybindings
bindkey -v

# Various PROMPT adjustments
local PS_STRING=""
if [[ "$USER" != "$DEFAULT_USER" ]]; then
    PS_STRING="${PS_STRING}%n"
fi
if [[ "$(HOSTNAME)" != "$DEFAULT_HOSTNAME" ]]; then
    PS_STRING="${PS_STRING}@%m"
fi
PS_STRING="${PS_STRING}:%c%(#.#.$) "
export PS1="$PS_STRING" #export PS1="%n@%m:%c%(#.#.$) "

# Aliases
[ -f ~/.aliases_general ] && source ~/.aliases_general
[ -f ~/.aliases_git ] && source ~/.aliases_git
[ -f ~/.aliases_ssh ] && source ~/.aliases_ssh

# Autocompletions
[ -f ~/.completions_tmuxinator.zsh ] && source ~/.completions_tmuxinator.zsh

# Helpers
command_exists () {
    type "$1" &> /dev/null ;
}

# Custom commands
dgrep() {
    # A recursive, case-insensitive grep that excludes binary files
    grep -iR "$@" * | grep -v "Binary"
}
if ! command_exists tree; then
	tree () {
		find $@ -print | sed -e 's;[^/]*/;|____;g;s;____|; |;g'
	}
fi

" VIMRC by Nicolai R. Tellefsen

" = Custom config
execute "silent! source $HOME/.vim/init_before.vim"

" ==== Startup settings ====
set runtimepath^=~/.vim
let mapleader="\<Space>"
set shortmess+=I

" ==== Plugin management ====
call plug#begin('~/.vim/plugged')

Plug 'ctrlpvim/ctrlp.vim'
Plug 'raimondi/delimitMate'
Plug 'eparreno/vim-l9'
Plug 'scrooloose/nerdtree'
"Plug 'neomake/neomake'
Plug 'w0rp/ale'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'mbbill/undotree'
Plug 'vim-airline/vim-airline'
"Plug 'othree/vim-autocomplete'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'hail2u/vim-css3-syntax'
Plug 'posva/vim-vue'
Plug 'othree/html5.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'sheerun/vim-polyglot'
Plug 'mattn/emmet-vim'
Plug 'editorconfig/editorconfig-vim'
Plug 'Shougo/context_filetype.vim' "Detection of filetype on a per code-block basis
Plug 'Shougo/echodoc.vim' "Echo data in doc line

" Colorschemes
Plug 'frankier/neovim-colors-solarized-truecolor-only'
Plug 'mhartington/oceanic-next'
Plug 'joshdick/onedark.vim'

" Typescript
Plug 'mhartington/nvim-typescript', { 'do': ':UpdateRemotePlugins' }

call plug#end()

" ==== General ====
" 1. Indents
set tabstop=4							"An indentation every four columns
set shiftwidth=4						"Use indents of 4 spaces
set softtabstop=4						"Let backspace delete indent
set modeline							"Accept vim modeline options when encountered
set noexpandtab							"Do not expand tabs by default
set listchars=tab:\|\ ,trail:·,extends:>,eol:¶
set scrolloff=10						"Keep x lines below and above cursor when scrolling
set mouse=a

" 2. Guides
"set omnifunc=syntaxcomplete#Complet		"Enable autocomplete
set showmatch							"Highlight matching brackets
set wildmode=longest:full,full			"Style of autocomplete display, alternative list:longest,full
set colorcolumn=80						"Display line-length guide

" 3. Presentation
set number								"Current line-number
set nowrap								"Don't wrap long lines
set comments=sl:/\*,mb:\*,elx:\*/		"auto format comment blocks (untested?)
set background=dark

if has("nvim")
	let $NVIM_TUI_ENABLE_TRUE_COLOR=1	"Just in case some plugins/schemes still uses this for reference
	set termguicolors					"Enable true color support?
	colorscheme solarized				"OceanicNext, onedark, solarized
else
	set t_Co=256
	colorscheme OceanicNext
endif

set guioptions-=L						"Disable left scrollbar
set guioptions-=r						"Disable right scrollbar
set guioptions-=m						"Hide menu
set guioptions-=T						"Hide toolbar

" 4. Technical
set undolevels=1000						"undo, undo, undo, etc...
set undoreload=10000					"save undo history
set hidden								"hide buffers instead of closing them
set ignorecase							"ignore case when searching
set smartcase							"ignore case only if searching in lowercase

if has("nvim")
	set inccommand=nosplit				"Live replace preview
endif

vnoremap < <gv
vnoremap > >gv

" ==== Directories ====
set directory=$HOME/.vim/swap//
set backupdir=$HOME/.vim/backup//
set viewdir=$HOME/.vim/views//
set undodir=$HOME/.vim/undo//
set undofile							"save undo history
au BufWinLeave \* silent! mkview		"make vim save view (state) (folds, cursor, etc)
au BufWinEnter \* silent! loadview		"make vim load view (state) (folds, cursor, etc)

" ==== Technical ====
set noautochdir							"disable automatic switching of directory
set viewoptions=folds,options,cursor,unix,slash
autocmd BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

" ==== Keymappings ====
" = Disable arrow keys
inoremap	<Up>	<NOP>
inoremap	<Down>	<NOP>
inoremap	<Left>	<NOP>
inoremap	<Right>	<NOP>
nnoremap	<Up>	<NOP>
nnoremap	<Down>	<NOP>
nnoremap	<Left>	<NOP>
nnoremap	<Right>	<NOP>

" = Map leader x/y/p to system clipboard
if has("win32") || has("win16")
	vnoremap <leader>y "*y
	nnoremap <leader>p "*p
	nnoremap <leader>P "*P
	vnoremap <leader>x "*x
else
	vnoremap <leader>y "+y
	nnoremap <leader>p "+p
	nnoremap <leader>P "+P
	vnoremap <leader>x "+x
endif

" = Use leader to save, load, etc
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>e :e<CR>

" = Use leader to toggle cursorline
:nnoremap <Leader>c :set cursorline!<CR>

" = Edit and reload this file
nmap <silent> <leader>ev :e $HOME/.vim/init.vim<CR>
nmap <silent> <leader>sv :so $HOME/.vim/init.vim<Cr>

" = Toggle gui visibility (will make window jump and resize)
nnoremap <C-F1> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>
nnoremap <C-F2> :if &go=~#'T'<Bar>set go-=T<Bar>else<Bar>set go+=T<Bar>endif<CR>
nnoremap <C-F3> :if &go=~#'r'<Bar>set go-=r<Bar>else<Bar>set go+=r<Bar>endif<CR>

" = Remove search highlights
nmap <silent> <leader>/ :nohlsearch<CR>
nmap <silent> <leader><leader> :nohlsearch<CR>

" = Show Undotree in split
nmap <leader>u :UndotreeToggle<CR>

" = Show NERDtree
map <leader>n :NERDTreeToggle<CR>

" = Move between splits
map <C-J> <C-W>j
map <C-K> <C-W>k
map <C-L> <C-W>l
map <C-H> <C-W>h

" = Move visually on wrapped lines
nnoremap j gj
nnoremap k gk

" = Neovim terminal
if has("nvim")
	tnoremap <Esc> <C-\><C-n>
	tnoremap <C-h> <C-\><C-n><C-w>h
	tnoremap <C-j> <C-\><C-n><C-w>j
	tnoremap <C-k> <C-\><C-n><C-w>k
	tnoremap <C-l> <C-\><C-n><C-w>l
endif

" = Omnicompletion
imap <c-space> <c-x><c-o>

" = Ale
let g:ale_completion_enabled = 1
let g:ale_history_log_output = 1
"let g:ale_typescript_tslint_executable = 'tslint -p . --type-check'

" = Deoplete
let g:deoplete#enable_at_startup = 1
let g:echodoc#enable_at_startup = 1
set noshowmode

" ==== Plugin settings ====
" = GitNote
nmap <leader>go :GnOpenLine<CR>
nmap <leader>gr :GnOpenReadme<CR>
nmap <leader>gp :GnOpenParent<CR>
nmap <leader>gh :GnOpenBaseReadme<CR>

" = Syntastic recommended defaults for newbs
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0

" = Neomake configuration
"autocmd! BufWritePost * Neomake
"let g:neomake_open_list = 2

" Disable phpcs checker for php files
"let g:syntastic_php_checkers = ['php']

" = UltiSnips recommended defaults
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" = Undotree
let g:undotree_SetFocusWhenToggle = 1

" = Airline
let g:airline_symbols = {}
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''

" = CtrlP
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_use_caching = 1
let g:ctrlp_clear_cache_on_exit = 0

" = EditorConfig
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

" = Add custom file extensions
au BufRead,BufNewFile *.variables setfiletype less
au BufRead,BufNewFile *.overrides setfiletype less

" = Custom config
execute "silent! source $HOME/.vim/init_after.vim"
